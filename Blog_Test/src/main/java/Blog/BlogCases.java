package Blog;

import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.lang.Thread.sleep;

/**
 * @author liuyandeng
 * @date 2023/10/17
 * @Description
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogCases extends InitAndEnd {


    /**
     * 输入正确的账号和密码，登录成功
     */
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "LoginSucc.csv")
    void LoginSucc(String username, String password, String blog_listUrl) throws InterruptedException {
//        System.out.println(username + password + blog_listUrl);
        // 打开博客页面
        webDriver.get("http://124.220.51.144:8020/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 输入账号 admin
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 输入密码 admin
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 点击提交按钮
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(5000);
        //弹窗 点击确定
        webDriver.switchTo().alert().accept();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        // 效验：跳转到列表页,获取当前的URL
        String cur_url = webDriver.getCurrentUrl();
        // 如果 获取的url 正确，则测试通过，否则测试失败
        Assertions.assertEquals(blog_listUrl, cur_url);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        // 效验：文章的名字是否一致
        webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > a:nth-child(4)")).click();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 列表页展示用户信息是 admin ，则测试通过，否则测试失败
        String cur_username = webDriver.findElement(By.cssSelector("#username")).getText();
        Assertions.assertEquals(username, cur_username);
    }

    /**
     * 博客列表页博客数量不为 0
     */
    @Order(2)
    @Test
    void Blog_list() {
        // 打开博客列表页
        webDriver.get("http://124.220.51.144:8020/myblog_list.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 获取页面上的所有博客标题对应元素
        int title_num = webDriver.findElements(By.cssSelector(".title")).size();
        // 如果元素数量不为0 ，测试通过
        Assertions.assertNotEquals(0, title_num);
    }

    /**
     * 博客详情页效验
     * url
     * 博客标题
     * 页面title是否是“博客正文"
     */
    private static Stream<Arguments> Generator() {
        return Stream.of(Arguments.arguments("http://124.220.51.144:8020/myblog_list.html",
                "博客列表",
                "第四篇文章"));
    }

    @Order(3)
    @ParameterizedTest
    @MethodSource("Generator")
    void BlogDetail(String expected_url, String expected_title, String expected_blogTitle) throws InterruptedException {
        // 1、找到第一篇博客对应的查看全文的按钮

        webDriver.get("http://124.220.51.144:8020/myblog_list.html");
        // 2、获取当前页面 url
        String cur_url = webDriver.getCurrentUrl();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        // 3、获取当前页面 title
        String cur_title = webDriver.getTitle();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        //4、获取博客标题
        String cur_BlogTile = webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > div.title")).getText();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        // 5、效验
        Assertions.assertEquals(expected_url, cur_url);
        Assertions.assertEquals(expected_title, cur_title);
//        Assertions.assertEquals(expected_blogTitle, cur_BlogTile);

    }

    /**
     * 写博客
     */
    @Order(4)
    @Test
    void editBlog() throws InterruptedException {
        // 1、找到写博客的按钮
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 2、通过JS来进行标题输入
        ((JavascriptExecutor) webDriver).executeScript("document.getElementById(\"title\").value=\"自动化测试\"");
        sleep(3000);
        // 3、点击发布
        webDriver.findElement(By.cssSelector("body > div.blog-edit-container > div.title > button")).click();
        sleep(3000);
        webDriver.switchTo().alert().dismiss();
        sleep(4000);
        // 4、获取当前 url
        String cur_url = webDriver.getCurrentUrl();
        Assertions.assertEquals("http://124.220.51.144:8020/myblog_list.html", cur_url);
    }

    /**
     * 校验已发布博客标题
     * 校验已发布博客时间
     */
    @Order(5)
    @Test
    void blogInfoCheck() {
        webDriver.get("http://124.220.51.144:8020/myblog_list.html");
        // 1、获取第一篇博客标题
        String first_blogTitle = webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > div.title")).getText();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 2、获取第一篇博客时间
        String first_blogTime = webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > div.date")).getText();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 3、校验博客标题是否是 自动化测试
        Assertions.assertEquals("自动化测试", first_blogTitle);
        // 4、如果时间是2023-10-17 则测试通过
        if (first_blogTime.contains("2023-10-17")) {
            System.out.println("测试通过");
        } else {
            System.out.println("当前时间：" + first_blogTime);
            System.out.println("测试不通过");
        }
    }

    /**
     * 删除刚刚发布的博客
     */
    @Order(6)
    @Test
    void delBlog() throws InterruptedException {
        webDriver.get("http://124.220.51.144:8020/myblog_list.html");
        // 删除指定的博客
        webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > a:nth-child(6)")).click();
        sleep(5000);
        webDriver.switchTo().alert().accept();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 博客列表页第一篇博客标题不是“自动化测试”
        String first_blogTitle= webDriver.findElement(By.xpath("//*[@id=\"artListDiv\"]/div[1]/div[1]")).getText();
        // 校验当前博客标题不等于“自动化测试”
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        if (first_blogTitle.equals("自动化测试")) {
            System.out.println("当前博客标题："+first_blogTitle);
            System.out.println("测试不通过");
        } else {
            System.out.println("删除刚刚发布博客，测试通过");
        }
//        Assertions.assertEquals(first_blogTitle,"自动化测试");

    }

    /**
     * 退出登录
     */
    @Order(7)
    @Test
    void logOut() throws InterruptedException {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
//        webDriver.get("http://124.220.51.144:8020/myblog_list.html");
        // 1、点击退出登录
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();
        sleep(3000);
        webDriver.switchTo().alert().accept();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        // 1、校验退出后是否是登录界面url
        String cur_url = webDriver.getCurrentUrl();
        Assertions.assertEquals("http://124.220.51.144:8020/login.html",cur_url);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);

        // 2、校验提交按钮
        WebElement webElement = webDriver.findElement(By.cssSelector("#submit"));
        Assertions.assertNotNull(webElement);
    }

}
